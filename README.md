# cir2turbo工具使用指南

本工具可以将`arkcompiler`生成的`circuit ir`转化为可供`v8`的可视化工具`turbolizer`使用的JSON文件

## 1. 访问turbolizer

### 1.1 直接访问

直接访问`https://netsh-f.github.io/turbolizer4cir2turbo/`，无需部署

### 1.2 自行部署（可选）

**部署和使用说明详见`turbolizer_for_cir2turbo`仓库**

请按照下面仓库`README.md`部署修改后的`turbolizer`。

仓库地址:

`https://gitee.com/JQKonatsu/turbolizer4cir2turbo`

## 2. 执行cir2turbo脚本

### 2.1 clone本仓库

```shell
git clone https://gitee.com/JQKonatsu/cir2turbo.git
```

### 2.2 准备日志文件

生成log需要在ark_aot_compiler命令行后面添加参数：`--compiler-log=all01 --log-level=info`

准备日志文件，放在项目目录中。

下面以`examples/helloaot.log`为例，该样例日志由`arkcompiler/ets_runtime/test/aottest/helloaot/helloaot.ts`编译生成。

### 2.3 执行命令

进入本仓库根目录，执行下面的命令

```shell
python cir2turbo.py examples/helloaot.log
```

该命令会在`out/`目录中生成`turbolizer`所需要的`json`文件

`out/helloaot.abc/0_func_main_0@helloaot.ts@354@helloaot.abc.json`

## 3. 导入json文件到turbolizer中

使用浏览器访问`turbolizer`服务（见`步骤1`）

使用快捷键`Ctrl+L`导入`步骤2.3`生成的json文件，即可显示出SON图。

更多`turbolizer`操作说明见本仓库WIKI

`https://gitee.com/JQKonatsu/cir2turbo/wikis`

## 开发说明

开发者可基于已有的源码对工具进行二次开发。

### control节点配置

目前control节点的判定采用白名单的形式，可通过修改`TurboNode.is_control_node()`方法来自定义control节点集合。

### 输入日志Log格式约束

本工具采用正则匹配的方法来识别Log中的信息，识别规则和其他约束如下：

首先识别各Phase

```
......=== phase_name [function_name@abc_name]......
...
...
...
......=== End ===......
```
在这两个Phase输出的开始和结尾标识之间的内容会被识别成为一个Phase，必须成对出现且非贪婪搜索，会匹配到最近的一对标识

之后在这两个标识之间，对每一行进行匹配，匹配到第一个`{`和`}`之间的内容，进行json元素解析，该json元素必须包含以下几个键

`id`, `op`, `MType`, `in`, `out`

例如
```
{"id":1, "op":"STATE_ENTRY", "MType":"NOVALUE, bitfield=0x0, type=EMPTY-gateType(0), stamp=0, mark=0, ","in":[[], [], [], [], [0]], "out":[19, 18]}
```

其中`MType`中各项目需要用`,`隔开，第一项是类型名，其余项需分别以变量名开头`bitfield=`, `type=`, `stamp=`, `mark=`。特别地，`bitfield=`项的值必须为合法十六进制数

`in`是一个list，其有5个list元素，含义按照下标分类为不同类型的边，分别为：
```
[
    [1],  # state_inputs
    [2, 3],  # depend_inputs
    [],  # value_inputs
    [],  # frame_state_inputs
    [0]   # root_inputs
]
```
其中的数字为该输入边的source节点的id

`out`为一个list，其中的数字为输出边的target节点id，其信息与in的信息是冗余的。

### 转换规则

本工具仅做格式上的转换，基本不改变Log输出图结构的信息，除一个特例：

当输入边的source节点不存在时，即该图中没有这个节点时，该输入边不会被生成。

## 其他

上文提到的`turbolizer`是基于`v8`项目的`turbolizer`修改而来的，针对`circuit ir`进行了适配，请使用修改后的`turbolizer`。

如出现“导入文件”或“重新布局”后页面无响应的情况，请提交issue及复现方法。